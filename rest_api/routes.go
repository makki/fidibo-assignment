package gin

import (
	"github.com/gin-gonic/gin"
)

func httpHandler(h *handler, r *gin.Engine) *gin.Engine {
	r.Use(h.checkAuth)
	r.POST("/search/book", h.search)
	return r
}
