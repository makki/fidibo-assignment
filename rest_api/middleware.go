package gin

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func (h *handler) checkAuth(c *gin.Context) {
	token := c.GetHeader("Authorization")
	if token == "" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Authorization header is not set"})
		return
	}
	tk := strings.TrimPrefix(token, "Bearer ")
	if !h.svc.IsAuthorized(c, tk) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "you are not authorized to use this service"})
		return
	}
}
