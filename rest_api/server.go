package gin

import (
	"context"
	"log"

	"assignment.fidibo.com/service"
	"github.com/gin-gonic/gin"
)

type server struct {
	handler *handler
}
type HealthFunctions map[string]func(context.Context) error

func NewServer(s *service.Search) server {
	h := &handler{svc: *s}
	return server{
		handler: h,
	}
}

func (s server) Serve(port string) error {
	routesHandler := func(r *gin.Engine) *gin.Engine {
		return httpHandler(s.handler, r)
	}
	runServer(routesHandler, port)
	return nil
}

func runServer(routesHandler func(r *gin.Engine) *gin.Engine, portNo string) {
	apiRouter := newEngine()
	routesHandler(apiRouter)
	err := apiRouter.Run(":" + portNo)
	if err != nil {
		log.Printf("error. server_running, %s", err.Error())
	}
}

func newEngine() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	engine := gin.New()
	engine.Use(gin.Recovery())
	return engine
}
