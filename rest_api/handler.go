package gin

import (
	"log"
	"net/http"

	"assignment.fidibo.com/service"
	"github.com/gin-gonic/gin"
)

type handler struct {
	svc service.Search
}

func NewHandler(s *service.Search) *handler {
	return &handler{
		svc: *s,
	}
}

func (h *handler) search(c *gin.Context) {
	phrase := c.Query("keyword")
	bb, err := h.svc.SearchBooks(c, phrase)
	if err != nil {
		log.Printf("error. handler_err: %s", err.Error())
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	if len(bb) == 0 {
		c.Status(http.StatusNoContent)
		return
	}
	c.JSON(http.StatusOK, bb)
}
