FROM golang:latest

RUN mkdir /app

# Set the Current Working Directory inside the container
WORKDIR /app

ADD . /app

# Build the Go app
RUN go build -o app/main

EXPOSE 8080

ENTRYPOINT ["./app/main"]