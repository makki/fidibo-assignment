package models

type Book struct {
	ID         string     `json:"id"`
	Title      string     `json:"title"`
	ImageName  string     `json:"image_name"`
	Publishers Publishers `json:"publishers"`
	Content    string     `json:"content"`
	Slug       string     `json:"slug"`
	Authors    []Author   `json:"authors"`
}

type Publishers struct {
	Title string `json:"title"`
}
type Author struct {
	Name string `json:"name"`
}
