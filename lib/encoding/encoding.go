package encoding

import (
	"bytes"
	"encoding/gob"
)

func Encode(m interface{}) (string, error) {
	target := &bytes.Buffer{}
	err := gob.NewEncoder(target).Encode(m)
	if err != nil {
		return "", err
	}
	return target.String(), nil
}

func Decode[T any](input string) (T, error) {
	var res T
	buf := bytes.NewBufferString(input)
	err := gob.NewDecoder(buf).Decode(&res)
	return res, err
}
