package httpex

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

var (
	instance *http.Client
	once     sync.Once
)

func Do(r Request) (result Response) {
	result.Request = r
	req, err := http.NewRequest(r.Method(), r.Url(), bytes.NewBuffer(r.Body()))
	if err != nil {
		result.Err = err
		return
	}
	setHeader(req, r.Headers())
	client := defaultClient(r.Timeout())

	start := time.Now()
	resp, err := client.Do(req)
	if err != nil {
		result.Err = err
		return
	}
	defer resp.Body.Close()

	result.ResponseTime = time.Since(start).Milliseconds()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		result.Err = err
		return
	}
	result.StatusCode = resp.StatusCode
	result.Header = resp.Header
	result.Body = string(data)
	return
}

func setHeader(req *http.Request, headers map[string]string) {
	for k, v := range headers {
		req.Header.Set(k, v)
	}
}

func defaultClient(timeout time.Duration) *http.Client {
	c := clientInstance()
	c.Timeout = timeout
	return c
}

func clientInstance() *http.Client {
	once.Do(
		func() {
			instance = createHttpClient()
		},
	)
	return instance
}

func createHttpClient() *http.Client {
	t := defaultTransport()
	c := &http.Client{
		Transport: t,
	}
	return c
}

func defaultTransport() *http.Transport {
	t, ok := http.DefaultTransport.(*http.Transport)
	if !ok {
		return nil
	}
	t.MaxIdleConns = 100
	t.MaxConnsPerHost = 0
	t.MaxIdleConnsPerHost = 100
	return t
}
