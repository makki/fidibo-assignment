package httpex

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type Request interface {
	Url() string
	Method() string
	Body() []byte
	Headers() map[string]string
	Timeout() time.Duration
}

type Response struct {
	Body         string      `json:"body"`
	Err          error       `json:"err"`
	ResponseTime int64       `json:"response_time"`
	StatusCode   int         `json:"status_code"`
	Header       http.Header `json:"header"`
	Request      Request     `json:"request"`
}

func (r *Response) Error() error {
	if r.Err != nil {
		return fmt.Errorf("request call failed, err: %w", r.Err)
	}
	if r.StatusCode < 200 || r.StatusCode >= 300 {
		return fmt.Errorf("the response status is not successful, code: %d", r.StatusCode)
	}
	return nil
}

func (r *Response) Unmarshal(i interface{}) error {
	b := r.Body
	if b == "" {
		b = `{}`
	}
	return json.Unmarshal([]byte(b), &i)
}

func (r *Response) Log() (res map[string]any) {
	res = map[string]any{
		"request": map[string]any{
			"url":     r.Request.Url(),
			"method":  r.Request.Method(),
			"body":    unmarshal(r.Request.Body()),
			"headers": r.Request.Headers(),
			"timeout": r.Request.Timeout().Milliseconds(),
		},
		"response": map[string]any{
			"body":         unmarshal([]byte(r.Body)),
			"statusCode":   r.StatusCode,
			"headers":      r.Header,
			"responseTime": r.ResponseTime,
		},
	}
	if r.Err != nil {
		res["response"].(map[string]any)["error"] = r.Err.Error()
	}
	return
}

func unmarshal(bb []byte) any {
	var res any
	if e := json.Unmarshal(bb, &res); e != nil {
		res = bb
	}
	return res
}
