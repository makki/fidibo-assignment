package main

import (
	"time"

	"assignment.fidibo.com/infra/auth"
	fidibo "assignment.fidibo.com/infra/fidibo_engine"
	"assignment.fidibo.com/infra/redis"
	"assignment.fidibo.com/service"

	gin "assignment.fidibo.com/rest_api"
)

func main() {
	url := "https://search.fidibo.com" // TODO get it from the config
	timeout := 1 * time.Minute         // TODO get it from the config
	engine := fidibo.New(url, timeout)
	cacheDb := redis.New("db", 6379) // TODO get host and port from the config
	authService := auth.New()
	svc := service.New(engine, cacheDb, authService)
	srv := gin.NewServer(svc)
	srv.Serve("8081") // TODO get the port from the config
}
