package infra

import (
	"context"
	"time"
)

type Cacher interface {
	Set(ctx context.Context, key string, value interface{}, ttl time.Duration) error
	Get(ctx context.Context, key string) (interface{}, error)
}
