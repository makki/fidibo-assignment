package redis

import (
	"context"
	"fmt"
	"time"

	"assignment.fidibo.com/infra"
	"assignment.fidibo.com/lib/encoding"
	"assignment.fidibo.com/models"
	"github.com/redis/go-redis/v9"
)

type cacheRepo struct {
	client *redis.Client
}

var _ infra.Cacher = &cacheRepo{}

func New(host string, port int) *cacheRepo {
	rdb := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%d", host, port),
	})
	return &cacheRepo{client: rdb}
}

func (c *cacheRepo) Set(ctx context.Context, key string, value interface{}, ttl time.Duration) error {
	value, err := encoding.Encode(value)
	if err != nil {
		return err
	}
	cmd := c.client.Set(ctx, key, value, ttl)
	return cmd.Err()
}

func (c *cacheRepo) Get(ctx context.Context, key string) (interface{}, error) {
	res := c.client.Get(ctx, key)
	if err := res.Err(); err != nil {
		return nil, err
	}
	s, err := res.Result()
	if err != nil {
		return nil, err
	}
	out, err := encoding.Decode[[]models.Book](s)
	if err != nil {
		return nil, err
	}
	return &out, nil
}
