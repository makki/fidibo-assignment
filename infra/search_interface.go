package infra

import (
	"context"

	"assignment.fidibo.com/models"
)

type Searcher interface{
	Search(ctx context.Context, query string) ([]models.Book, error)
}