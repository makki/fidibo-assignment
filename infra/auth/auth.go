package auth

import (
	"context"

	"assignment.fidibo.com/infra"
)

type service struct{}

var _ infra.Authorizer = &service{}

func New() *service {
	return &service{}
}

func (s *service) IsAuthorized(ctx context.Context, jwt string) bool {
	return true
}
