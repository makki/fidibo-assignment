package fidibo

type document struct {
	Books      books         `json:"books"`
	Authors    authors       `json:"authors"`
	Publishers publishersRes `json:"publishers"`
	Categories categories    `json:"categories"`
}
type shards struct {
	Total      int `json:"total"`
	Successful int `json:"successful"`
	Skipped    int `json:"skipped"`
	Failed     int `json:"failed"`
}
type publishers struct {
	Title string `json:"title"`
}
type author struct {
	Name string `json:"name"`
}
type source struct {
	ImageName  string     `json:"image_name"`
	Publishers publishers `json:"publishers"`
	Weight     int        `json:"weight"`
	ID         string     `json:"id"`
	Title      string     `json:"title"`
	Content    string     `json:"content"`
	Slug       string     `json:"slug"`
	Authors    []author   `json:"authors"`
}
type innerHits struct {
	Index  string  `json:"_index"`
	Type   string  `json:"_type"`
	ID     string  `json:"_id"`
	Score  float64 `json:"_score"`
	Source source  `json:"_source"`
}
type hits struct {
	Total    int         `json:"total"`
	MaxScore float64     `json:"max_score"`
	Hits     []innerHits `json:"hits"`
}
type books struct {
	Took     int    `json:"took"`
	TimedOut bool   `json:"timed_out"`
	Shards   shards `json:"_shards"`
	Hits     hits   `json:"hits"`
}
type authors struct {
	Took     int    `json:"took"`
	TimedOut bool   `json:"timed_out"`
	Shards   shards `json:"_shards"`
	Hits     hits   `json:"hits"`
}
type publishersRes struct {
	Took     int    `json:"took"`
	TimedOut bool   `json:"timed_out"`
	Shards   shards `json:"_shards"`
	Hits     hits   `json:"hits"`
}
type categories struct {
	Took     int    `json:"took"`
	TimedOut bool   `json:"timed_out"`
	Shards   shards `json:"_shards"`
	Hits     hits   `json:"hits"`
}
