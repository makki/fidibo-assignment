package fidibo

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	repository "assignment.fidibo.com/infra"
	httpex "assignment.fidibo.com/lib/http_client"
	"assignment.fidibo.com/models"
)

type searchEngine struct {
	url     string
	timeout time.Duration
}

var _ repository.Searcher = &searchEngine{}

func New(url string, t time.Duration) *searchEngine {
	return &searchEngine{url: url, timeout: t}
}

func (s *searchEngine) Search(ctx context.Context, query string) ([]models.Book, error) {
	req := NewRequest(s.url, query, s.timeout)
	resp := httpex.Do(req)
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("fetching books from server failed. status code = %d", resp.StatusCode)
	}
	bb := getBooks(resp.Body)
	return bb, nil
}

func getBooks(resp string) []models.Book {
	doc := document{}
	err := json.Unmarshal([]byte(resp), &doc)
	if err != nil {
		return nil
	}
	bb := doc.Books.Hits.Hits
	res := make([]models.Book, 0, len(bb))
	for i := range bb {
		b := bb[i].Source
		tmp := models.Book{
			ImageName:  b.ImageName,
			ID:         b.ID,
			Title:      b.Title,
			Content:    b.Content,
			Slug:       b.Slug,
			Publishers: models.Publishers{Title: b.Publishers.Title},
			Authors:    []models.Author{},
		}
		for _, a := range b.Authors {
			tmp.Authors = append(tmp.Authors, models.Author{Name: a.Name})
		}
		res = append(res, tmp)
	}
	return res
}
