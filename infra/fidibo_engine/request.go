package fidibo

import (
	"net/http"
	"net/url"
	"time"

	httpex "assignment.fidibo.com/lib/http_client"
)

type request struct {
	url     string
	query   string
	timeout time.Duration
}

func NewRequest(url, query string, t time.Duration) *request {
	return &request{url: url, query: query, timeout: t}
}

var _ httpex.Request = &request{}

func (r *request) Url() string {
	return r.url
}

func (r *request) Method() string {
	return http.MethodPost
}

func (r *request) Body() []byte {
	formData := url.Values{
		"q": {r.query},
	}
	return []byte(formData.Encode())
}

func (r *request) Headers() map[string]string {
	return map[string]string{"Content-Type": "application/x-www-form-urlencoded"}
}

func (r *request) Timeout() time.Duration {
	return r.timeout
}
