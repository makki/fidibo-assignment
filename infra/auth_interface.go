package infra

import "context"

type Authorizer interface {
	IsAuthorized(ctx context.Context, jwt string) bool
}
