package service

import (
	"context"

	"assignment.fidibo.com/infra"
	"assignment.fidibo.com/models"
)

type Search struct {
	searchEngine infra.Searcher
	cacher       infra.Cacher
	auth         infra.Authorizer
}

func New(se infra.Searcher, cacher infra.Cacher, auth infra.Authorizer) *Search {
	return &Search{
		searchEngine: se,
		cacher:       cacher,
		auth:         auth,
	}
}

func (s *Search) SearchBooks(ctx context.Context, query string) ([]models.Book, error) {
	bb, err := s.searchEngine.Search(ctx, query)
	return bb, err
}

func (s *Search) IsAuthorized(ctx context.Context, jwt string) bool {
	return s.auth.IsAuthorized(ctx, jwt)
}
